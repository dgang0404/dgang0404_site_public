import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueLodash from "vue-lodash";
import lodash from "lodash";
import VueSwal from "vue-swal";
import "./vee-validate";

Vue.use(VueLodash, { name: "custom", lodash: lodash });
Vue.use(VueSwal);
Vue.config.productionTip = false;

new Vue({
    router,
    store,
    vuetify,
    render: (h) => h(App),
}).$mount("#app");