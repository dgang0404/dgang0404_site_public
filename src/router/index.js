import Vue from "vue";
import VueRouter from "vue-router";
import home from "../views/home";
import music from "../views/music";
import inv from "../views/inv";
import maze from "../views/maze";
import about from "../views/about";
import store from "../store/index";

Vue.use(VueRouter);

const routes = [{
        path: "/home",
        name: "home",
        component: home,
    },
    {
        path: "/music",
        name: "music",
        component: music,
    },
    {
        path: "/invform",
        name: "invform",
        component: inv,
    },
    {
        path: "/maze",
        name: "maze",
        component: maze,
    },
    {
        path: "/putterstuf",
        name: "putterstuf",
        component: about,
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
    scrollBehavior() {
        window.scrollTo(0, 0);
        document.getElementById("app").scrollIntoView();
        return { x: 0, y: 0 };
    },
});
export default router;