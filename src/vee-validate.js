import { extend, localize } from "vee-validate";
import {
    min,
    max,
    numeric,
    required,
    length,
    email,
    alpha,
    alpha_spaces,
    // date_format,
} from "vee-validate/dist/rules";
import en from "vee-validate/dist/locale/en.json";
import { storeInx } from "../src/store/index";
import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

// Install rules
extend("required", required);
extend("min", min);
extend("max", max);
extend("alpha", alpha);
extend("length", length);
extend("numeric", numeric);
extend("email", email);
extend("alpha_spaces", alpha_spaces);
//extend("date_format", date_format);

localize({
    en,
});