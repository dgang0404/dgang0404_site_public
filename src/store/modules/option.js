import lodash from "lodash";

function getHighest() {
    var last = state[0].valNidObjs.length;
    var curr = null;
    var highest = 0;
    for (var i = 0; i < state.length; i++) {
        curr = state[i].valNidObjs.length;
        if (curr <= last) {
            highest = last;
        } else {
            highest = curr;
            last = highest;
        }
    }
    return highest;
}

function getBlankValNidObjs(key) {
    var obj = {
        value: null,
        keyid: key,
    };
    return obj;
}

const state = [{
        id: "componentlink",
        rules: "",
        valNidObjs: [{
            value: null,
            keyid: 1,
        }, ],
    },
    {
        id: "onOff",
        rules: "",
        valNidObjs: [{
            value: null,
            keyid: 1,
        }, ],
    },
    {
        id: "notes",
        rules: "",
        valNidObjs: [{
            value: null,
            keyid: 1,
        }, ],
    },
    {
        id: "startTime",
        rules: "", //date_format:hh:mm
        valNidObjs: [{
            value: null,
            keyid: 1,
        }, ],
    },
    {
        id: "endTime",
        rules: "",
        valNidObjs: [{
            value: null,
            keyid: 1,
        }, ],
    },
    {
        id: "duration",
        rules: "",
        valNidObjs: [{
            value: null,
            keyid: 1,
        }, ],
    },
];

const mutations = {
    SET_Opt(state, dat) {
        var foundKey = false;
        //var foundId = false;
        var highest = getHighest();
        var obj = {
            value: dat.value,
            keyid: dat.keyid,
        };
        for (var i = 0; i < state.length; i++) {
            var valNidObjsLength = state[i].valNidObjs.length;
            for (var x = 0; x < valNidObjsLength; x++) {
                //update
                if (
                    dat.keyid == state[i].valNidObjs[x].keyid &&
                    dat.id == state[i].id
                ) {
                    state[i].valNidObjs[x].value = dat.value;
                    foundKey = true;
                }

                if (x + 1 >= valNidObjsLength) {
                    // add a new row
                    if (!foundKey && dat.id == state[i].id) {
                        state[i].valNidObjs.push(obj);
                        highest = getHighest();
                    } else if (
                        //push blank val into state for the next component to show up
                        (highest <= dat.keyid &&
                            state[i].valNidObjs[valNidObjsLength - 1].value != null &&
                            state[i].valNidObjs[valNidObjsLength - 1].value != "") ||
                        state[i].valNidObjs.length < highest
                    ) {
                        var blank = getBlankValNidObjs(state[i].valNidObjs.length + 1);
                        state[i].valNidObjs.push(blank);
                        highest = getHighest();
                        i = 0;
                        x = -1;
                    }
                }
            }
        }
    },
    SET_DeleteOpt(state, keyid) {
        var tempState = state;

        for (var i = 0; i < tempState.length; i++) {
            for (var x = 0; x < tempState[i].valNidObjs.length; x++) {
                if (
                    (x > 1 || tempState[i].valNidObjs.length > 1) &&
                    x != tempState[i].valNidObjs.length
                ) {
                    var dis = lodash.remove(tempState[i].valNidObjs, {
                        keyid: keyid,
                    });
                }
            }
        }
        state = tempState;
        //reset the keys
        for (var i = 0; i < state.length; i++) {
            for (var x = 0; x < state[i].valNidObjs.length; x++) {
                state[i].valNidObjs[x].keyid = x + 1;
            }
        }
    },
    RESET_OPT_LINK(state, payload) {
        for (let i = 0; i < state.length; i++) {
            if (state[i].id == "componentlink") {
                for (let x = 0; x < state[i].valNidObjs.length; x++) {
                    if (
                        state[i].valNidObjs[x].keyid == payload.keyid &&
                        state[i].valNidObjs[x].value != null
                    ) {
                        state[i].valNidObjs[x].value = payload.value;
                    }
                }
            }
        }
    },
    SET_OPT_FROM_MASTER(state, payload) {
        //this will be called from the "load" or search fetcher
        var componentlink = [];
        var onOffs = [];
        var notes = [];
        var startTimes = [];
        var endTimes = [];
        var durations = [];
        var tempState = state;
        //set single arrays
        for (var i = 0; i < payload.lineItem.length; i++) {
            if (
                payload.lineItem[i].onOff === undefined &&
                payload.lineItem[i].notes === undefined &&
                payload.lineItem[i].startTime === undefined &&
                payload.lineItem[i].endTime === undefined &&
                payload.lineItem[i].duration === undefined
            ) {
                console.log("no option for this item");
            } else {
                componentlink.push(payload.lineItem[i].component);
                onOffs.push(payload.lineItem[i].onOff);
                notes.push(payload.lineItem[i].notes);
                startTimes.push(payload.lineItem[i].startTime);
                endTimes.push(payload.lineItem[i].endTime);
                durations.push(payload.lineItem[i].duration);
            }
        }
        //set temp state could proly do it in nested loop...GOOOwa fff0or it dood
        //just loop the temp state and switch on the id to put the li ... still guna ba a bigger switch statment whatever, its fine
        for (var i = 0; i < componentlink.length; i++) {
            var kid = i;
            kid++;
            var valNidObj = { value: componentlink[i], keyid: kid };
            if (onOffs.length <= 1 || i < 1) {
                tempState[0].valNidObjs[0] = valNidObj;
            } else if (onOffs.length == kid) {
                tempState[0].valNidObjs.push(valNidObj);
                //push blank row
                kid++;
                var valNidObj = { value: null, keyid: kid };
                tempState[0].valNidObjs.push(valNidObj);
            } else {
                tempState[0].valNidObjs.push(valNidObj);
            }
        }
        for (var i = 0; i < onOffs.length; i++) {
            var kid = i;
            kid++;
            var valNidObj = { value: onOffs[i], keyid: kid };
            if (onOffs.length <= 1 || i < 1) {
                tempState[1].valNidObjs[0] = valNidObj;
            } else if (onOffs.length == kid) {
                tempState[1].valNidObjs.push(valNidObj);
                //push blank row
                kid++;
                var valNidObj = { value: null, keyid: kid };
                tempState[1].valNidObjs.push(valNidObj);
            } else {
                tempState[1].valNidObjs.push(valNidObj);
            }
        }
        for (var i = 0; i < notes.length; i++) {
            var kid = i;
            kid++;
            var valNidObj = { value: notes[i], keyid: kid };
            if (notes.length <= 1 || i < 1) {
                tempState[2].valNidObjs[0] = valNidObj;
            } else if (notes.length == kid) {
                tempState[2].valNidObjs.push(valNidObj);
                //push blank row
                kid++;
                var valNidObj = { value: null, keyid: kid };
                tempState[2].valNidObjs.push(valNidObj);
            } else {
                tempState[2].valNidObjs.push(valNidObj);
            }
        }
        for (var i = 0; i < startTimes.length; i++) {
            var kid = i;
            kid++;
            var valNidObj = { value: startTimes[i], keyid: kid };
            if (startTimes.length <= 1 || i < 1) {
                tempState[3].valNidObjs[0] = valNidObj;
            } else if (startTimes.length == kid) {
                tempState[3].valNidObjs.push(valNidObj);
                //push blank row
                kid++;
                var valNidObj = { value: null, keyid: kid };
                tempState[3].valNidObjs.push(valNidObj);
            } else {
                tempState[3].valNidObjs.push(valNidObj);
            }
        }
        for (var i = 0; i < endTimes.length; i++) {
            var kid = i;
            kid++;
            var valNidObj = { value: endTimes[i], keyid: kid };
            if (endTimes.length <= 1 || i < 1) {
                tempState[4].valNidObjs[0] = valNidObj;
            } else if (endTimes.length == kid) {
                tempState[4].valNidObjs.push(valNidObj);
                //push blank row
                kid++;
                var valNidObj = { value: null, keyid: kid };
                tempState[4].valNidObjs.push(valNidObj);
            } else {
                tempState[4].valNidObjs.push(valNidObj);
            }
        }
        for (var i = 0; i < durations.length; i++) {
            var kid = i;
            kid++;
            var valNidObj = { value: durations[i], keyid: kid };
            if (durations.length <= 1 || i < 1) {
                tempState[5].valNidObjs[0] = valNidObj;
            } else if (durations.length == kid) {
                tempState[5].valNidObjs.push(valNidObj);
                //push blank row
                kid++;
                var valNidObj = { value: null, keyid: kid };
                tempState[5].valNidObjs.push(valNidObj);
            } else {
                tempState[5].valNidObjs.push(valNidObj);
            }
        }
        //set the state with the temp state. doing it this way makes this stuff reactive
        state = tempState;
    },
    CLEAR_OPT_STATE(state) {
        var initialState = [{
                id: "componentlink",
                rules: "",
                valNidObjs: [{
                    value: null,
                    keyid: 1,
                }, ],
            },
            {
                id: "onOff",
                rules: "",
                valNidObjs: [{
                    value: null,
                    keyid: 1,
                }, ],
            },
            {
                id: "notes",
                rules: "",
                valNidObjs: [{
                    value: null,
                    keyid: 1,
                }, ],
            },
            {
                id: "startTime",
                rules: "",
                valNidObjs: [{
                    value: null,
                    keyid: 1,
                }, ],
            },
            {
                id: "endTime",
                rules: "",
                valNidObjs: [{
                    value: null,
                    keyid: 1,
                }, ],
            },
            {
                id: "duration",
                rules: "",
                valNidObjs: [{
                    value: null,
                    keyid: 1,
                }, ],
            },
        ];
        Object.assign(state, initialState);
    },
};

const actions = {
    setOpt({ commit }, payload) {
        commit("SET_Opt", payload);
    },
    setDeleteOpt({ commit }, keyid) {
        commit("SET_DeleteOpt", keyid);
    },
    setCompFromOption({ commit }, payload) {
        commit("SET_OPT_FROM_MASTER", payload);
    },
    resetOptLinkName({ commit }, payload) {
        commit("RESET_OPT_LINK", payload);
    },
    clearOptState({ commit }, payload) {
        commit("CLEAR_OPT_STATE", payload);
    },
};

const getters = {
    getOpts(state) {
        return state;
    },
};

export default {
    state,
    mutations,
    actions,
    getters,
};