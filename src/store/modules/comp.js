import lodash from "lodash";
import Vue from "vue";
//for adding the blank row
function getHighest() {
    var last = state[0].valNidObjs.length;
    var curr = null;
    var highest = 0;
    for (var i = 0; i < state.length; i++) {
        curr = state[i].valNidObjs.length;
        if (curr <= last) {
            highest = last;
        } else {
            highest = curr;
            last = highest;
        }
    }
    return highest;
}
//creating blank row
function getBlankValNidObjs(key) {
    var obj = {
        value: null,
        keyid: key,
    };
    return obj;
}
//value and id objects = valNidObjs
const state = [{
        id: "component",
        rules: "required",
        valNidObjs: [{
            value: null,
            keyid: 1,
        }, ],
    },
    {
        id: "amount",
        rules: "required|numeric",
        valNidObjs: [{
            value: null,
            keyid: 1,
        }, ],
    },
    {
        id: "unitOfMeasurement",
        rules: "required",
        valNidObjs: [{
            value: null,
            keyid: 1,
        }, ],
    },
];
const mutations = {
    SET_COMP(state, dat) {
        var foundKey = false;
        var highest = getHighest();
        //new value insures a blank row will be at the end of the array
        var obj = {
            value: dat.value,
            keyid: dat.keyid,
        };
        for (var i = 0; i < state.length; i++) {
            var valNidObjsLength = state[i].valNidObjs.length;
            for (var x = 0; x < valNidObjsLength; x++) {
                //update
                if (
                    dat.keyid == state[i].valNidObjs[x].keyid &&
                    dat.id == state[i].id
                ) {
                    state[i].valNidObjs[x].value = dat.value;
                    foundKey = true;
                }

                if (x + 1 >= valNidObjsLength) {
                    // add a new row
                    if (!foundKey && dat.id == state[i].id) {
                        state[i].valNidObjs.push(obj);
                        highest = getHighest();
                    } else if (
                        //push blank val into state for the next component to show up
                        (highest <= dat.keyid &&
                            state[i].valNidObjs[valNidObjsLength - 1].value != null &&
                            state[i].valNidObjs[valNidObjsLength - 1].value != "") ||
                        state[i].valNidObjs.length < highest
                    ) {
                        var blank = getBlankValNidObjs(state[i].valNidObjs.length + 1);
                        state[i].valNidObjs.push(blank);
                        highest = getHighest();
                        i = 0;
                        x = -1;
                    }
                }
            }
        }
    },
    //using getters here usually is bad but now the comp delete is self contained
    //corresponding options will be deleted
    SET_DELETE(state, { keyid, getters, dispatch }) {
        //delete corresponding option : child delete
        for (var i = 0; i < state.length; i++) {
            for (var x = 0; x < state[i].valNidObjs.length; x++) {
                //get component value for linkedge
                var compLinkVal = null;
                if (
                    state[i].id == "component" &&
                    state[i].valNidObjs[x].keyid == keyid
                ) {
                    compLinkVal = state[i].valNidObjs[x].value;
                    var opts = getters.getOpts;
                    // find matching comp
                    opts.forEach((e) => {
                        if (e.id == "componentlink") {
                            e.valNidObjs.forEach((obj) => {
                                if (obj.value == compLinkVal) {
                                    dispatch("setDeleteOpt", obj.keyid);
                                }
                            });
                        }
                    });
                }
            }
        }
        //delete comp : parent delete
        var tempState = state;
        for (var i = 0; i < tempState.length; i++) {
            for (var x = 0; x < tempState[i].valNidObjs.length; x++) {
                //check to see if we are deleting the first line or new input line
                if (
                    (x > 1 || tempState[i].valNidObjs.length > 1) &&
                    x != tempState[i].valNidObjs.length
                ) {
                    var dis = lodash.remove(tempState[i].valNidObjs, {
                        keyid: keyid,
                    });
                }
            }
        }

        state = tempState;
        //reset the keys
        for (var i = 0; i < state.length; i++) {
            for (var x = 0; x < state[i].valNidObjs.length; x++) {
                state[i].valNidObjs[x].keyid = x + 1;
            }
        }
    },
    SET_COMP_FROM_MASTER(state, payload) {
        //this will be called from the "load" or search fetcher
        var components = [];
        var amounts = [];
        var unitOfMeasurements = [];
        var tempState = state;

        //set single arrays
        for (var i = 0; i < payload.lineItem.length; i++) {
            components.push(payload.lineItem[i].component);
            amounts.push(payload.lineItem[i].amount);
            unitOfMeasurements.push(payload.lineItem[i].unitOfMeasurement);
        }
        //set temp state
        //...i knnoew it could be a one nested loop
        for (var i = 0; i < components.length; i++) {
            var kid = i;
            kid++;
            var valNidObj = { value: components[i], keyid: kid };
            if (components.length <= 1 || i < 1) {
                tempState[0].valNidObjs[0] = valNidObj;
            } else if (components.length == kid) {
                tempState[0].valNidObjs.push(valNidObj);
                //push blank row
                kid++;
                var valNidObj = { value: null, keyid: kid };
                tempState[0].valNidObjs.push(valNidObj);
            } else {
                tempState[0].valNidObjs.push(valNidObj);
            }
        }
        for (var i = 0; i < amounts.length; i++) {
            var kid = i;
            kid++;
            var valNidObj = { value: amounts[i], keyid: kid };
            if (amounts.length <= 1 || i < 1) {
                tempState[1].valNidObjs[0] = valNidObj;
            } else if (amounts.length == kid) {
                tempState[1].valNidObjs.push(valNidObj);
                //push blank row
                kid++;
                var valNidObj = { value: null, keyid: kid };
                tempState[1].valNidObjs.push(valNidObj);
            } else {
                tempState[1].valNidObjs.push(valNidObj);
            }
        }
        for (var i = 0; i < unitOfMeasurements.length; i++) {
            var kid = i;
            kid++;
            var valNidObj = { value: unitOfMeasurements[i], keyid: kid };
            if (unitOfMeasurements.length <= 1 || i < 1) {
                tempState[2].valNidObjs[0] = valNidObj;
            } else if (unitOfMeasurements.length == kid) {
                tempState[2].valNidObjs.push(valNidObj);
                //push blank row
                kid++;
                var valNidObj = { value: null, keyid: kid };
                tempState[2].valNidObjs.push(valNidObj);
            } else {
                tempState[2].valNidObjs.push(valNidObj);
            }
        }
        //set the state with the temp state. doing it this way makes this stuff reactive
        state = tempState;
    },
    CLEAR_COMP_STATE(state) {
        var initialState = [{
                id: "component",
                rules: "required",
                valNidObjs: [{
                    value: null,
                    keyid: 1,
                }, ],
            },
            {
                id: "amount",
                rules: "required|numeric",
                valNidObjs: [{
                    value: null,
                    keyid: 1,
                }, ],
            },
            {
                id: "unitOfMeasurement",
                rules: "required",
                valNidObjs: [{
                    value: null,
                    keyid: 1,
                }, ],
            },
        ];
        Object.assign(state, initialState);
    },
};

const actions = {
    setComp({ commit }, payload) {
        commit("SET_COMP", payload);
    },
    setDelete({ commit, getters, dispatch }, keyid) {
        commit("SET_DELETE", { keyid, getters, dispatch });
    },
    setCompFromMaster({ commit }, payload) {
        commit("SET_COMP_FROM_MASTER", payload);
    },
    clearCompState({ commit }) {
        commit("CLEAR_COMP_STATE");
    },
};

const getters = {
    getterComps(state) {
        return state;
    },
};

export default {
    state,
    mutations,
    actions,
    getters,
};