const state = {
    uid: null,
    userName: "",
    password: "",
    email: "",
    isLoggedIn: false,
};

const mutations = {
    SET_USER(state, payload) {
        state.uid = payload.uid;
        state.userName = payload.userName;
        state.password = payload.password;
        state.email = payload.email;
        state.isLoggedIn = payload.isLoggedIn;
    },
};

const actions = {
    setUser({ commit }, payload) {
        commit("SET_INV", payload);
    },
};

const getters = {
    getUser(state) {
        return state;
    },
};

export default {
    state,
    mutations,
    actions,
    getters,
};