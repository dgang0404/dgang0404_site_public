const state = {
    invId: null,
    uid: null,
    title: "",
    description: "",
    createdDate: "",
    highlightedRow: null,
};

const mutations = {
    SET_INV(state, dat) {
        switch (dat.id) {
            case "invId":
                state.invId = dat.value;
                break;
            case "title":
                state.title = dat.value;
                break;
            case "description":
                state.description = dat.value;
                break;
            case "createdDate":
                state.createdDate = dat.value;
                break;
        }
    },
    SET_INV_FROM_LOAD(state, dat) {
        state.invId = dat.id;
        state.title = dat.title;
        state.description = dat.description;
        state.createdDate = dat.createdDate;
    },
    SET_HIGHLIGHTED(state, keyid) {
        state.highlightedRow = keyid;
    },
    CLEAR_INV_STATE(state) {
        var initialState = {
            invId: null,
            uid: null,
            title: "",
            description: "",
            createdDate: "",
            highlightedRow: null,
        };
        Object.assign(state, initialState);
    },
};

const actions = {
    setInv({ commit }, payload) {
        commit("SET_INV", payload);
    },
    setInvFromLoad({ commit }, payload) {
        commit("SET_INV_FROM_LOAD", payload);
    },
    setHigh({ commit }, keyid) {
        commit("SET_HIGHLIGHTED", keyid);
    },
    clearInvState({ commit }) {
        commit("CLEAR_INV_STATE");
    },
};

const getters = {
    getInv(state) {
        return state;
    },
    getHigh(state) {
        return state.highlightedRow;
    },
};

export default {
    state,
    mutations,
    actions,
    getters,
};