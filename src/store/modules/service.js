import axios from "axios";
import querystring from "querystring";
const state = {
    user: {
        id: null,
        userName: "",
        password: "",
        email: "",
        isLoggedIn: false,
    },
    invId: null,
    uid: null,
    title: "",
    description: "",
    createdDate: "",
    lineItem: [{
        component: null,
        amount: null,
        unitOfMeasurement: null,
        onOff: null,
        notes: null,
        startTime: null,
        endTime: null,
        duration: null,
        keyid: 1,
    }, ],
};

const mutations = {
    SET_USER(state, payload) {
        state.user.uid = payload.uid;
        state.user.userName = payload.userName;
        state.user.password = payload.password;
        state.user.email = payload.email;
        state.user.isLoggedIn = payload.isLoggedIn;
    },
    SET_SERVICE_OBJ_INV(state, payload) {
        state.title = payload.title;
        state.description = payload.description;
        state.createdDate = payload.createdDate;
        state.expirationDate = payload.expirationDate;
    },
    SET_SERVICE_OBJ_LI(state, { payload, getters }) {
        if (payload == null) {
            console.log("null dat SET_SERVICE_OBJ_LI");
            return;
        }

        //get list of option component link keyids in the order we need for master
        var clKeyid = [];
        if (payload[0].id != "component") {
            var comps = getters.getterComps;
            var valuecl = null;
            //use the complink value to match the corresponding comp value and pass the comp keyid as the option keyid
            for (var i = 0; i < payload.length; i++) {
                for (var x = 0; x < payload[i].valNidObjs.length; x++) {
                    //set li or push li
                    if (payload[i].id == "componentlink") {
                        comps.forEach((comp) => {
                            if (comp.id == "component") {
                                comp.valNidObjs.forEach((compobj) => {
                                    valuecl = compobj.value;
                                    if (payload[i].valNidObjs[x].value == valuecl) {
                                        clKeyid.push(compobj.keyid);
                                    }
                                });
                            }
                        });
                    }
                }
            }
        }

        for (var i = 0; i < payload.length; i++) {
            for (var x = 0; x < payload[i].valNidObjs.length - 1; x++) {
                var value = payload[i].valNidObjs[x].value;
                if (state.lineItem[x] == null) {
                    //push new row
                    var li = {
                        component: null,
                        amount: null,
                        unitOfMeasurement: null,
                        onOff: null,
                        notes: null,
                        startTime: null,
                        endTime: null,
                        duration: null,
                        keyid: 1,
                    };
                    li.keyid = x + 1;
                    state.lineItem.push(li);
                }
                var optKeyid = clKeyid[x] - 1;
                //set li or push li
                if (payload[i].id == "component") {
                    state.lineItem[x].component = value;
                } else if (payload[i].id == "amount") {
                    state.lineItem[x].amount = value;
                } else if (payload[i].id == "unitOfMeasurement") {
                    state.lineItem[x].unitOfMeasurement = value;
                } else if (payload[i].id == "onOff") {
                    state.lineItem[optKeyid].onOff = value;
                } else if (payload[i].id == "notes") {
                    state.lineItem[optKeyid].notes = value;
                } else if (payload[i].id == "startTime") {
                    state.lineItem[optKeyid].startTime = value;
                } else if (payload[i].id == "endTime") {
                    state.lineItem[optKeyid].endTime = value;
                } else if (payload[i].id == "duration") {
                    state.lineItem[optKeyid].duration = value;
                }
            }
        }
    },
    POST_INV(state) {
        axios
            .post(
                "https://sd-inventory-535c1-default-rtdb.firebaseio.com/inventory.json",
                state
            )
            .then(function(response) {
                //handle success
                console.log(response);
                //get id from response
                const status = JSON.parse(response.status);
                if (status == "200") {}
            })
            .catch(function(response) {
                //handle error
                console.log(response);
            });
    },
    GET_USER(state, payload) {
        var url =
            "https://sd-inventory-535c1-default-rtdb.firebaseio.com/inventory.json?" +
            'orderBy="user/userName"' +
            '&equalTo="' +
            payload.userName +
            '"' +
            "&limitToLast=1" +
            "&print=pretty";
        axios
            .get(url)
            .then(function(response) {
                //handle success
                console.log(response);
                //get id from response
                const status = JSON.parse(response.status);
                if (status == "200") {}

                var results = {
                    id: null,
                    userName: "",
                    password: "",
                };
                for (const id in response.data) {
                    results.id = id;
                    results.userName = response.data[id].user.userName;
                    results.password = response.data[id].user.password;
                }
                if (
                    results.userName == payload.userName &&
                    results.password == payload.password
                ) {
                    state.user.userName = results.userName;
                    state.user.password = results.password;
                    state.user.isLoggedIn = true;
                } else {
                    state.user.isLoggedIn = false;
                }
            })
            .catch(function(response) {
                //handle error
                console.log(response);
            });
    },
};

const actions = {
    setUser({ commit }, payload) {
        commit("SET_USER", payload);
    },
    setServeObjInv({ commit }, payload) {
        commit("SET_SERVICE_OBJ_INV", payload);
    },
    setServeObjLineItem({ commit, getters }, payload) {
        commit("SET_SERVICE_OBJ_LI", { payload, getters });
    },
    postInv({ commit }) {
        commit("POST_INV");
    },
    fetchUser({ commit }, payload) {
        commit("GET_USER", payload);
    },
};

const getters = {
    getMastInv(state) {
        return state;
    },
    getUser(state) {
        return state.user;
    },
};

export default {
    state,
    mutations,
    actions,
    getters,
};