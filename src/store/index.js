import Vue from "vue";
import Vuex from "vuex";
import inv from "../store/modules/inventory";
import comp from "../store/modules/comp";
import opt from "../store/modules/option";
import service from "../store/modules/service";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        inv,
        comp,
        opt,
        service,
    },
});